<?php 
include_once("includes/header.php");
require_once("config/config.php");

// echo $_SESSION["username"];

if(!isset($_SESSION["username"])){
	header("Location: ./login.php");
	exit(); 
}

if(isset($_POST['delete'])){
	  // print_r($_POST);
	$db1=db();
	$stmDel = $db1->prepare("DELETE 
		FROM users
		WHERE id=?;");
	$stmDel->bind_param('i', $_POST['user_id']);
	$stmDel->execute();
}

$db = db();
$query = "SELECT * FROM `users` WHERE role='employee'";
$result = mysqli_query($db,$query) or die(mysql_error());
$rows = mysqli_num_rows($result);

$error = array();

if(isset($_POST['add'])){

	if(empty($_POST['name'])) {
		array_push($error,'Name is required');
	}
	if (empty($_POST['email'])) {
		array_push($error,'Email is required');
	}
	if (empty($_POST['password'])) {
		array_push($error,'Password is required');
	}

	if (count($error)>0) {

		//Validation Error . . 
	
	} else{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$db=db();
		$check_email_query = "SELECT 'email' FROM users WHERE email=?";
		$result = $db->prepare($check_email_query);
		$result->bind_param('s', $email);
		$result->execute();
		$result->store_result();

		if ($result->num_rows > 0) {
			array_push($error,'email already taken !');
		} else {

			$con=db();
			$stmIn = $con->prepare("INSERT INTO users(name,email,password,role) 
				VALUES (?, ?, ?, ?)");
			$employee = "employee";
			$stmIn->bind_param('ssss',$name,$email, md5($password),$employee);
			$stmIn->execute();
			?>
			<script type="text/javascript"> window.location="./index.php"; </script>

			<?php
		}
	}
}
?>


	<?php 
	if (count($error)>0) {
		echo "<ul class=' col-md-4 offset-md-4 mt-1'>";
		foreach ($error as $value) {

			echo "<li class='alert alert-danger'>".$value."</li>";
		}
		echo "</ul>";
	}
	?>


<div class="col-md-7 offset-md-3" style="margin-top: 5%">
	<form action="index.php" method="POST">
		<div class="form-row align-items-center">
			<input type="hidden" name="id">
			<div class="col-auto">
				<label class="sr-only" for="inlineFormInput">Name</label>
				<input type="text" class="form-control mb-2" id="name" name="name" placeholder="Employee Name" required="">
			</div>
			<div class="col-auto">
				<label class="sr-only" for="inlineFormInput">Email</label>
				<input type="email" class="form-control mb-2" id="position" name="email" placeholder="Employee Email" required="">
			</div>
			<div class="col-auto">
				<label class="sr-only" for="inlineFormInput">Password</label>
				<input type="password" class="form-control mb-2" id="password" name="password" placeholder="Default Password" required="">
			</div>
			<div class="col-auto">
				<input type="submit" class="btn btn-primary mb-2" name="add" id="add" value="Add Employee" />
			</div>
		</div>
	</form>
</div>

<div class="col-md-7 offset-md-3">

<?php if ($rows>0) { ?>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php while ($user = mysqli_fetch_assoc($result)) {
				echo "<tr>
					<td>".$user['name']."</td>
					<td>".$user['email']."</td>
					<td>
					<form action='password_change.php' method='POST'>
						<input type='hidden' name='user_id' value='".$user['id']."'>
						<input class='btn btn-info' type='submit' name='change_password' value='change password'>
					</form>
					</td>
					<td>
					<form action='edit.php' method='POST'>
						<input type='hidden' name='user_id' value='".$user['id']."'>
						<input class='btn btn-info' type='submit' name='edit' value='Edit'>
					</form>

					</td>
					<td>
					<form action='' method='POST'>
						<input type='hidden' name='user_id' value='".$user['id']."'>
						<input class='btn btn-danger' type='submit' name='delete' value='delete'>
					</form>

					</td>
					</tr>";
			} ?>
			
		</tbody>
	</table>

<?php } ?>
	

</div>


<?php
include("includes/footer.php");
?>