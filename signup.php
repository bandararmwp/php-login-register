<?php 
include_once("includes/header.php");
require_once("config/config.php");


if(isset($_SESSION["username"])){
	header("Location: ./index.php");
	exit(); 
}

$name = $email = $password = "";
$error = array();

if(isset($_POST['signup'])){

	if(empty($_POST['name'])) {
		array_push($error,'Name is required');
	}
	if (empty($_POST['email'])) {
		array_push($error,'Email is required');
	}
	if (empty($_POST['password'])) {
		array_push($error,'Password is required');
	}
	if (empty($_POST['re_password'])) {
		array_push($error,'Confirm Password is required');
	}
	if ($_POST['password'] != $_POST['re_password']) {
		array_push($error,'password does not match');
	}

	if (count($error)>0) {

		//Validation Error . . 
	
	} else{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$re_password = $_POST['re_password'];

		$db=db();
		$check_email_query = "SELECT 'email' FROM users WHERE email=?";
		$result = $db->prepare($check_email_query);
		$result->bind_param('s', $email);
		$result->execute();
		$result->store_result();

		if ($result->num_rows > 0) {
			array_push($error,'email already taken !');
		} else {

			$con=db();
			$stmIn = $con->prepare("INSERT INTO users(name,email,password,role) 
				VALUES (?, ?, ?, ?)");
			$employee = "employee";
			$stmIn->bind_param('ssss',$name,$email, md5($password),$employee);
			$stmIn->execute();
			$_SESSION['username'] = $name;
			?>
			<script type="text/javascript"> window.location="./index.php"; </script>

			<?php
		}
	}
}
?>

<div class="">
	<?php 
	if (count($error)>0) {
		echo "<ul class=' col-md-4 offset-md-4 mt-1'>";
		foreach ($error as $value) {

			echo "<li class='alert alert-danger'>".$value."</li>";
		}
		echo "</ul>";
	}
	?>
	<div class="col-md-4 offset-md-4 mt-4">
		<h2 class="alert alert-success" role="alert">
			Sign Up
		</h2>
		<form action="" method="POST">
			<div class="form-group">
				<label>Name : </label>
				<input type="text" class="form-control" id="name" name="name"  placeholder="Enter Name" required="true">
			</div>

			<div class="form-group">
				<label>Email : </label>
				<input type="email" class="form-control" id="email" name="email"  placeholder="Enter email" required="true">
			</div>
			<div class="form-group">
				<label>Password :</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required="true">
			</div>

			<div class="form-group">
				<label>Retype-Password :</label>
				<input type="password" class="form-control" id="re_password" name="re_password" placeholder="Retype-Password" required="true">
			</div>

			<label>Already Have Account ? </label>
			<a href="login.php"> Login </a>
			<button type="submit" class="btn btn-success float-right" name="signup">Sign Up</button>
		</form>
	</div>
</div>


<?php 
include_once("includes/footer.php");
?>