<?php 
include_once("includes/header.php");
require_once("config/config.php");


if(!isset($_SESSION["username"])){
	header("Location: ./index.php");
	exit(); 
}

if (isset($_POST['edit'])) {

	$db = db();
	$id = $_POST['user_id'];
	$query = "SELECT * FROM `users` WHERE id='$id'";
	$result = mysqli_query($db,$query) or die(mysql_error());
	$user = mysqli_fetch_assoc($result);
	$rows = mysqli_num_rows($result);
}

$error = array();

if(isset($_POST['update'])){

	if(empty($_POST['name'])) {
		array_push($error,'Name is required');
	}
	if (empty($_POST['email'])) {
		array_push($error,'Email is required');
	}
	if (empty($_POST['password'])) {
		array_push($error,'Password is required');
	}
	if (empty($_POST['re_password'])) {
		array_push($error,'Confirm Password is required');
	}
	if ($_POST['password'] != $_POST['re_password']) {
		array_push($error,'password does not match');
	}

	if (count($error)>0) {

		//Validation Error . . 

	} else{
		$db=db();
		$stmUp = $db->prepare("UPDATE employees 
			SET name=?, email=?, password=?, role=?  WHERE id=? ");
		$employee = "employee";
		$stmUp->bind_param('ssssi',$name,$email,$password,$employee, $_POST['id']);
		$stmUp->execute();

		?>
		<script type="text/javascript"> window.location="./index.php"; </script>

		<?php
	}

}
?>

<div class="">
	<?php 
	if (count($error)>0) {
		echo "<ul class=' col-md-4 offset-md-4 mt-1'>";
		foreach ($error as $value) {

			echo "<li class='alert alert-danger'>".$value."</li>";
		}
		echo "</ul>";
	}
	?>
	<div class="col-md-4 offset-md-4 mt-4">
		<h2 class="alert alert-success" role="alert">
			Edit user
		</h2>
		<form action="" method="POST">
			<div class="form-group">
				<label>Name : </label>
				<input type="text" class="form-control" id="name" name="name"  placeholder="Enter Name" required="true" value="<?php echo($user['name']) ?>">
			</div>

			<div class="form-group">
				<label>Email : </label>
				<input type="email" class="form-control" id="email" name="email"  placeholder="Enter email" required="true" value="<?php echo($user['email']) ?>">
			</div>
			<div class="form-group">
				<label>Password :</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required="true">
			</div>

			<div class="form-group">
				<label>Retype-Password :</label>
				<input type="password" class="form-control" id="re_password" name="re_password" placeholder="Retype-Password" required="true">
			</div>

			<input type="hidden" name="id" value="<?php echo($_POST['user_id']) ?>">
			<button type="submit" class="btn btn-success float-right" name="update">Update</button>
		</form>
	</div>
</div>


<?php 
include_once("includes/footer.php");
?>