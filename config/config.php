<?php 



define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','omobio_test');

function db(){
		try{
		$connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME); 
		if ($connection){
			echo '<script>console.log("DB is Connected")</script>';

			// create user table
			$sql_user = "CREATE TABLE users(
				id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
				name VARCHAR(255) NOT NULL,
				email VARCHAR(255) NOT NULL,
				password VARCHAR(255) NOT NULL,
				role VARCHAR(255) NOT NULL
				);";
			if(mysqli_query($connection,$sql_user)) {

				echo '<script>console.log("Created user table")</script>'; 
			} else{ 
				echo '<script>console.log("User table already exists")</script>'; 
			}

			// create employee table
			// $sql_employee = "CREATE TABLE employees(
			// 	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
			// 	name VARCHAR(255)NOT NULL,
			// 	position VARCHAR(255)NOT NULL,
			// 	salary VARCHAR(255) NOT NULL
			// 	);";
			// if(mysqli_query($connection,$sql_employee)) {
			// 	echo '<script>console.log("Created employee table")</script>'; 
			// } else{
			// 	echo '<script>console.log("Employee table already exists")</script>'; 
			// }

		} else {
			echo "error : DB isnot connected";
		}
	}catch (exception  $e){
		exit("Error: " . $e->getMessage());
	}

	return $connection;
}

db();

?>
