<?php 
include_once("includes/header.php");
require_once("config/config.php");

if(isset($_SESSION["username"])){
	header("Location: ./index.php");
	exit(); 
}

$error = array();

if(isset($_POST['login'])){

	if (empty($_POST['email'])) {
		array_push($error,'Email is required');
	}
	if (empty($_POST['password'])) {
		array_push($error,'Password is required');
	}

	if (count($error)>0) {
			// have error
			// print_r( count($error) );	
	} else{
		$db = db();
		$email = stripslashes($_REQUEST['email']);
		$email = mysqli_real_escape_string($db,$email);

		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($db,$password);

		$query = "SELECT * FROM `users` WHERE email='$email' and password='".md5($password)."'";
		$result = mysqli_query($db,$query);
		$user = mysqli_fetch_array($result);

		$rows = mysqli_num_rows($result);
		if($rows==1){
			$_SESSION['id'] = $user['id'];
			$_SESSION['username'] = $user['name'];
			?>
			<script type="text/javascript"> window.location = "./index.php"; </script>

			<?php

		}else{
			array_push($error,'Error Login. Please Enter Valid email and password');
		}
	}
}
?>

<div class="">
	<?php 
	if (count($error)>0) {
		echo "<ul class=' col-md-4 offset-md-4 mt-1'>";
		foreach ($error as $value) {

			echo "<li class='alert alert-danger'>".$value."</li>";
		}
		echo "</ul>";
	}
	?>
	<div class="col-md-4 offset-md-4 mt-3">
		<h2 class="alert alert-success" role="alert">
			Login
		</h2>
		<form action="" method="POST">
			<div class="form-group">
				<label>Email : </label>
				<input type="email" class="form-control" id="email" name="email"  placeholder="Enter email" required="true">
			</div>
			<div class="form-group">
				<label>Password :</label>
				<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required="true">
			</div>
			<label>Don't Have Account ? </label>
			<a href="signup.php"> Sign Up</a>
			<input type="submit" class="btn btn-success float-right" name="login" value="Submit">
		</form>
	</div>
</div>


<?php 
include_once("includes/footer.php");
?>

