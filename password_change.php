<?php 
include_once("includes/header.php");
require_once("config/config.php");

// echo $_SESSION["username"];

// echo $_POST['user_id'];

if(!isset($_SESSION["username"])){
	header("Location: ./login.php");
	exit(); 
}

$error = array();

if(isset($_POST['change'])){

	if (empty($_POST['current_password'])) {
		array_push($error,'Current Password is required');
	}
	if (empty($_POST['new_password'])) {
		array_push($error,'new_password is required');
	}
	if (empty($_POST['re_type_new_password'])) {
		array_push($error,'Confirm Password is required');
	}
	if ($_POST['new_password'] != $_POST['re_type_new_password']) {
		array_push($error,'password does not match');
	}

	$db=db();

	$id = $_REQUEST['id'];

	$password = stripslashes($_REQUEST['current_password']);
	$password = mysqli_real_escape_string($db,$password);

	$query = "SELECT * FROM `users` WHERE id='$id' and password='".md5($password)."'";
	$result = mysqli_query($db,$query);
	$user = mysqli_fetch_array($result);

	$rows = mysqli_num_rows($result);

	if ($rows==0) {
		array_push($error,'current password does not match');
	}


	if (count($error)>0) {

		//Validation Error . . 
		
	} else{

		$db=db();
		$new_password = stripslashes($_REQUEST['new_password']);
		$new_password = mysqli_real_escape_string($db,$new_password);
		$new_password = md5($new_password);

		$update_query = "UPDATE users SET password='$new_password' WHERE id='$id'";
		$result = mysqli_query($db,$update_query);

		?>

		<script type="text/javascript"> window.location="./index.php"; </script>

		<?php
	}
}

?>

<?php 
if (count($error)>0) {
	echo "<ul class=' col-md-4 offset-md-4 mt-1'>";
	foreach ($error as $value) {

		echo "<li class='alert alert-danger'>".$value."</li>";
	}
	echo "</ul>";
}
?>

<div class="col-md-4 offset-md-4 mt-3">
	<h2 class="alert alert-success" role="alert">
		Change Password
	</h2>
	<form action="" method="POST">
		<div class="form-group">
			<label>Current Password : </label>
			<input type="password" class="form-control" id="current_password" name="current_password"  placeholder="Enter current password" required="true">
		</div>
		<div class="form-group">
			<label>New Password : </label>
			<input type="password" class="form-control" id="new_password" name="new_password"  placeholder="Enter new password" required="true">
		</div>
		<div class="form-group">
			<label>Re Type New Password : </label>
			<input type="password" class="form-control" id="re_type_new_password" name="re_type_new_password"  placeholder="Enter re type new password" required="true">
		</div>
		<input type="hidden" name="id" value="<?php echo $_POST['user_id']; ?>">
		<input type="submit" class="btn btn-success float-right" name="change" value="Change">
	</form>
</div>





<?php
include("includes/footer.php");
?>