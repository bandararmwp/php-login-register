<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Title</title>
	<link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
				</li>
			</ul>

			<?php if(isset($_SESSION["username"])){ ?>

			<div class="form-inline my-2 my-lg-0">
				<a class="mr-sm-2"><?php echo $_SESSION["username"]; ?></a>
				<a href="logout.php" class="form-control mr-sm-2">Logout</a>
			</div>

			<?php 
			} else { ?>

			<div class="form-inline my-2 my-lg-0">
				<a href="login.php" class="form-control mr-sm-2">Login</a>
				<a href="signup.php" class="form-control mr-sm-2">Sign Up</a>
			</div>

			<?php  } ?>
		</div>
	</nav>